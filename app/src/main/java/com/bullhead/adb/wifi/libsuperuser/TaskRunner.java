package com.bullhead.adb.wifi.libsuperuser;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

public class TaskRunner {

    private static TaskRunner instance;
    private Thread thread;
    private Handler handler;

    private TaskRunner() {
        handler = new Handler(Looper.getMainLooper());
    }

    public static TaskRunner getInstance() {
        if (instance == null) {
            instance = new TaskRunner();
        }
        return instance;
    }

    public void run(@Nullable OnResultListener listener, @NonNull String... commands) {
        if (thread != null) {
            thread.interrupt();
            thread = null;
        }
        thread = new Thread(() -> {
            List<String> result = Shell.SU.run(commands);
            if (listener != null) {
                handler.post(() -> listener.onResult(result));
            }
        });
        thread.start();
    }

    public void checkAvailability(@NonNull OnAvailableListener listener) {
        if (thread != null) {
            thread.interrupt();
            thread = null;
        }
        thread = new Thread(() -> {
            if (Shell.SU.available()) {
                handler.post(listener::onAvailable);
            } else {
                handler.post(listener::onUnavailable);
            }
        });
        thread.start();
    }


    @FunctionalInterface
    public interface OnResultListener {
        void onResult(List<String> t);
    }

    public interface OnAvailableListener {
        void onAvailable();

        void onUnavailable();
    }

}
